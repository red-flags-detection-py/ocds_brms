FROM maven:3.5.2-jdk-8-alpine AS MAVEN_BUILD
MAINTAINER Matias Rivas

COPY pom.xml /build/
COPY src /build/src/
COPY rules /build/rules/
WORKDIR /build/
RUN mvn package

FROM openjdk:8-jre-alpine
WORKDIR /app

COPY --from=MAVEN_BUILD /build/target/*.jar /app/target/app.jar
COPY --from=MAVEN_BUILD /build/src /app/src
COPY --from=MAVEN_BUILD /build/rules /app/rules
ENTRYPOINT ["sh", "-c", "java -jar target/app.jar"]