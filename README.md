# Proyecto de implementacion de un BRMS
El ocds_brms es una implementacion de DROOLS como BRMS sobre el conjunto de datos de contrataciones publicas para la investigacion 'Red Flags Detection in Public Procurements Using a Business Rules Management System'

## Licencia
Esta obra está bajo una licencia de Creative Commons Reconocimiento 4.0 Internacional.

![CC BY 4.0!](https://i.creativecommons.org/l/by/4.0/88x31.png)
