package com.ocds.brms;

import static org.elasticsearch.client.RequestOptions.DEFAULT;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;

import org.drools.core.SessionConfiguration;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.ClearScrollResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.HttpAsyncResponseConsumerFactory;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieSessionConfiguration;
import org.kie.api.runtime.rule.FactHandle;
import org.kie.internal.io.ResourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocds.models.Result;

@Service
public class BrmsService {


	final static Logger logger = LoggerFactory.getLogger(BrmsService.class);

	private static RequestOptions COMMON_OPTIONS;

	static int BUFFER_SIZE = 200 * 1024 * 1024;

	@Autowired
	private Config config;

	private KieSession session;

	public void init() {
		logger.info("Inicializando KieSession...");
		try {
			session = getKieSession();
		} catch (Exception e) {
			logger.error("Falló init..");
			e.printStackTrace();
		}
	}

	public void scan(boolean fullScan) throws IOException, NullPointerException {

		// seteamos un tiempo inicial
		long startTime = System.currentTimeMillis();

		// asignamos el cliente
		RestHighLevelClient client = config.client();

		QueryBuilder qb;
		// construimos la query

		if(fullScan) {
		 	qb = QueryBuilders.matchAllQuery();
		 	logger.info("Full Scan");
		} else {
		 	qb = QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery("procesado"));
			logger.info("Partial Scan");
		}
		
		// queries para prueba en desarrollo
//		qb = QueryBuilders.queryStringQuery("ocds-03ad3f-280197-1").defaultField("doc.ocid");
//		QueryBuilder qb = QueryBuilders.queryStringQuery("ocds-03ad3f-292018-1").defaultField("doc.ocid");
//		QueryBuilder qb = QueryBuilders.queryStringQuery("true").defaultField("doc.compiledRelease.tender.hasEnquiries");

		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		searchSourceBuilder.query(qb);
		searchSourceBuilder.size(1000); // cantidad max de hits que seran retornados por cada scroll

		final Scroll scroll = new Scroll(TimeValue.timeValueMinutes(5L));

		SearchRequest searchRequest = new SearchRequest("ocds");
		searchRequest.scroll(scroll);
		searchRequest.source(searchSourceBuilder);

//			RequestOptions.Builder builder;

		RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder();

		// The default cache size is 100 MiB. Change it to 30 MiB.
		builder.setHttpAsyncResponseConsumerFactory(
				new HttpAsyncResponseConsumerFactory
						.HeapBufferedResponseConsumerFactory(BUFFER_SIZE));
		COMMON_OPTIONS = builder.build();

		// consultamos al elasticsearch
		SearchResponse searchResponse = client.search(searchRequest, COMMON_OPTIONS);

		ObjectMapper mapper = new ObjectMapper();
		
		Long count = new Long(0);


		// Scroll hasta que no se retornen mas hits
		do {
			for (SearchHit hit : searchResponse.getHits().getHits()) {
				Result result = mapper.readValue(hit.getSourceAsString(), Result.class);
				if(result.getDoc()!= null){
					count += 1;
					logger.info(count.toString() + "    " + result.getDoc().getOcid());
					try {
						FactHandle handle = session.insert(result.getDoc().getCompiledRelease());
						session.fireAllRules();
						session.delete(handle);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
					UpdateRequest updateRequest = new UpdateRequest("ocds", "record", hit.getId());
					if(result.getDoc().getCompiledRelease().getRedFlags() != null) {
						String jsonString = result.getDoc().getCompiledRelease().getRedFlags().toString();
						String banderas = "{\"banderas\":" + jsonString + ", \"procesado\":1" + "}";
						updateRequest.doc(banderas, XContentType.JSON);
						client.update(updateRequest, COMMON_OPTIONS);
					} else {
						updateRequest.doc("procesado", 1);
						client.update(updateRequest, COMMON_OPTIONS);
					}


				}
			}
			SearchScrollRequest scrollRequest = new SearchScrollRequest(searchResponse.getScrollId()).scroll(scroll);
			searchResponse = client.scroll(scrollRequest, COMMON_OPTIONS);
		} while (searchResponse.getHits().getHits().length != 0); // Cero hits marca el final del scroll y el while

		logger.info("Proceso finalizado exitosamente");
		long endTime = System.currentTimeMillis() - startTime;

		// tiempo en horas, minutos, segundos
		String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(endTime),
				TimeUnit.MILLISECONDS.toMinutes(endTime) % TimeUnit.HOURS.toMinutes(1),
				TimeUnit.MILLISECONDS.toSeconds(endTime) % TimeUnit.MINUTES.toSeconds(1));
		logger.info("Tiempo total: " + hms);

		ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
		clearScrollRequest.addScrollId(searchResponse.getScrollId());
		ClearScrollResponse clearScrollResponse = client.clearScroll(clearScrollRequest, COMMON_OPTIONS);
		boolean succeeded = clearScrollResponse.isSucceeded();
	}

	private KieSession getKieSession() throws Exception {
		KieServices kieServices = KieServices.Factory.get();
		KieFileSystem kfs = kieServices.newKieFileSystem();
		
		Path rutaReglas = Paths.get("/app/rules").resolve("reglas.xls").toAbsolutePath();
		File archivoReglas = rutaReglas.toFile();
		kfs.write(ResourceFactory.newFileResource(archivoReglas));

		//kfs.write(ResourceFactory.newClassPathResource("reglas.xls"));

		KieBuilder kieBuilder = kieServices.newKieBuilder(kfs).buildAll();
		KieContainer kieContainer = kieServices.newKieContainer(kieServices.getRepository().getDefaultReleaseId());
		KieSessionConfiguration conf = SessionConfiguration.newInstance();
		KieSession ksession = kieContainer.newKieSession(conf);
		if (kieBuilder.getResults().hasMessages(Message.Level.ERROR)) {
			List<Message> errors = kieBuilder.getResults().getMessages(Message.Level.ERROR);
			StringBuilder sb = new StringBuilder("Errors:");
			for (Message msg : errors) {
				sb.append("\n  " + msg);
			}
			try {
				throw new Exception(sb.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return ksession;
	}

}
