package com.ocds.brms;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;

import javax.annotation.PreDestroy;

import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.devtools.filewatch.FileSystemWatcher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;

@Configuration
public class Config {


    final static Logger logger = LoggerFactory.getLogger(Config.class);
	
	@Autowired
	private BrmsService brms;

    @Bean
    public RestHighLevelClient client() {
        ClientConfiguration clientConfiguration
                = ClientConfiguration.builder()
                .connectedTo("matirivas.me:9200")
                .withBasicAuth("elastic", "admin" )
                .build();

        return RestClients.create(clientConfiguration).rest();
    }
    
    @Bean
    public FileSystemWatcher fileSystemWatcher() {
        FileSystemWatcher fileSystemWatcher = new FileSystemWatcher(true, Duration.ofMillis(5000L), Duration.ofMillis(3000L));
        //fileSystemWatcher.addSourceFolder(new File("src/main/resources"));
		Path rutaReglas = Paths.get("/app/rules").toAbsolutePath();
        fileSystemWatcher.addSourceFolder(rutaReglas.toFile());
        fileSystemWatcher.addListener(new MyFileChangeListener(brms));
        fileSystemWatcher.start();
        logger.info("FileSystemWatcher Started");
        return fileSystemWatcher;
    }

    @PreDestroy
    public void onDestroy() throws Exception {
        fileSystemWatcher().stop();
    }

}