package com.ocds.brms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class OcdsBrmsApplication implements CommandLineRunner {

	final static Logger logger = LoggerFactory.getLogger(OcdsBrmsApplication.class);

	@Autowired
	private BrmsService service;



	public static void main(String[] args) {
		SpringApplication.run(OcdsBrmsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info("Running...");
		service.init();
		service.scan(Boolean.FALSE);
	}

}
