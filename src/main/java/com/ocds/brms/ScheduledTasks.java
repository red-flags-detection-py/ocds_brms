package com.ocds.brms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

@Component
public class ScheduledTasks {

	@Autowired
	private BrmsService brms;

	private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");



	// ejecuta usando una cron expression
	// se ejecuta todos los dias a media noche
	@Scheduled(cron = "0 0 0 * * *")
	public void scheduleTaskWithCronExpression() {
	    logger.info("Cron Task :: Execution Time - {}", dateTimeFormatter.format(LocalDateTime.now()));
		try {
			brms.scan(Boolean.FALSE);
		} catch (NullPointerException | IOException e) {
			logger.error("No se pudo realizar proceso cron {}", e);
		}
	}

}