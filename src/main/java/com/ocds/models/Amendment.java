
package com.ocds.models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Amendment {

	@JsonProperty("date")
	private String date;
	@JsonProperty("rationale")
	private String rationale;
	@JsonProperty("id")
	private String id;
	@JsonProperty("description")
	private String description;
	@JsonProperty("amendsReleaseID")
	private String amendsReleaseID;
	@JsonProperty("releaseID")
	private String releaseID;
	@JsonProperty("amendsAmount")
	private Value amendsAmount;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Amendment() {
	}

	public Amendment(String date, String rationale, String id, String description, String amendsReleaseID,
			String releaseID, Value amendsAmount, Map<String, Object> additionalProperties) {
		super();
		this.date = date;
		this.rationale = rationale;
		this.id = id;
		this.description = description;
		this.amendsReleaseID = amendsReleaseID;
		this.releaseID = releaseID;
		this.amendsAmount = amendsAmount;
		this.additionalProperties = additionalProperties;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("date")
	public String getDate() {
		return date;
	}

	@JsonProperty("date")
	public void setDate(String date) {
		this.date = date;
	}

	@JsonProperty("rationale")
	public String getRationale() {
		return rationale;
	}

	@JsonProperty("rationale")
	public void setRationale(String rationale) {
		this.rationale = rationale;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("amendsReleaseID")
	public String getAmendsReleaseID() {
		return amendsReleaseID;
	}

	@JsonProperty("amendsReleaseID")
	public void setAmendsReleaseID(String amendsReleaseID) {
		this.amendsReleaseID = amendsReleaseID;
	}

	@JsonProperty("releaseID")
	public String getReleaseID() {
		return releaseID;
	}

	@JsonProperty("releaseID")
	public void setReleaseID(String releaseID) {
		this.releaseID = releaseID;
	}

	@JsonProperty("amendsAmount")
	public Value getAmendsAmount() {
		return amendsAmount;
	}

	@JsonProperty("amendsAmount")
	public void setAmendsAmount(Value amendsAmount) {
		this.amendsAmount = amendsAmount;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
