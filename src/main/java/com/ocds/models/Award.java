
package com.ocds.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "status",
    "statusDetails",
    "date",
    "value",
    "suppliers",
    "documents",
    "items"
})
public class Award {

    @JsonProperty("id")
    private String id;
    @JsonProperty("status")
    private String status;
    @JsonProperty("statusDetails")
    private String statusDetails;
    @JsonProperty("date")
    private String date;
    @JsonProperty("value")
    private Value value;
    @JsonProperty("suppliers")
    private List<Supplier> suppliers = null;
    @JsonProperty("documents")
    private List<Document> documents = null;
    @JsonProperty("items")
    private List<Item> items = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Award() {
    }

    /**
     * 
     * @param date
     * @param suppliers
     * @param documents
     * @param statusDetails
     * @param id
     * @param value
     * @param items
     * @param status
     */
    public Award(String id, String status, String statusDetails, String date, Value value, List<Supplier> suppliers, List<Document> documents, List<Item> items) {
        super();
        this.id = id;
        this.status = status;
        this.statusDetails = statusDetails;
        this.date = date;
        this.value = value;
        this.suppliers = suppliers;
        this.documents = documents;
        this.items = items;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("statusDetails")
    public String getStatusDetails() {
        return statusDetails;
    }

    @JsonProperty("statusDetails")
    public void setStatusDetails(String statusDetails) {
        this.statusDetails = statusDetails;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("value")
    public Value getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(Value value) {
        this.value = value;
    }

    @JsonProperty("suppliers")
    public List<Supplier> getSuppliers() {
        return suppliers;
    }

    @JsonProperty("suppliers")
    public void setSuppliers(List<Supplier> suppliers) {
        this.suppliers = suppliers;
    }

    @JsonProperty("documents")
    public List<Document> getDocuments() {
        return documents;
    }

    @JsonProperty("documents")
    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    @JsonProperty("items")
    public List<Item> getItems() {
        return items;
    }

    @JsonProperty("items")
    public void setItems(List<Item> items) {
        this.items = items;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
