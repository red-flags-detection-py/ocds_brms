
package com.ocds.models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "period",
    "measures",
    "classifications",
    "financialCode"
})
public class Breakdown {

    @JsonProperty("id")
    private String id;
    @JsonProperty("period")
    private Period period;
    @JsonProperty("measures")
    private Measures measures;
    @JsonProperty("classifications")
    private Classifications classifications;
    @JsonProperty("financialCode")
    private String financialCode;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Breakdown() {
    }

    /**
     * 
     * @param classifications
     * @param period
     * @param measures
     * @param financialCode
     * @param id
     */
    public Breakdown(String id, Period period, Measures measures, Classifications classifications, String financialCode) {
        super();
        this.id = id;
        this.period = period;
        this.measures = measures;
        this.classifications = classifications;
        this.financialCode = financialCode;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("period")
    public Period getPeriod() {
        return period;
    }

    @JsonProperty("period")
    public void setPeriod(Period period) {
        this.period = period;
    }

    @JsonProperty("measures")
    public Measures getMeasures() {
        return measures;
    }

    @JsonProperty("measures")
    public void setMeasures(Measures measures) {
        this.measures = measures;
    }

    @JsonProperty("classifications")
    public Classifications getClassifications() {
        return classifications;
    }

    @JsonProperty("classifications")
    public void setClassifications(Classifications classifications) {
        this.classifications = classifications;
    }

    @JsonProperty("financialCode")
    public String getFinancialCode() {
        return financialCode;
    }

    @JsonProperty("financialCode")
    public void setFinancialCode(String financialCode) {
        this.financialCode = financialCode;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
