
package com.ocds.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "description",
    "amount",
    "budgetBreakdown"
})
public class Budget {

    @JsonProperty("description")
    private String description;
    @JsonProperty("amount")
    private Amount amount;
    @JsonProperty("budgetBreakdown")
    private List<BudgetBreakdown> budgetBreakdown = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Budget() {
    }

    /**
     * 
     * @param amount
     * @param budgetBreakdown
     * @param description
     */
    public Budget(String description, Amount amount, List<BudgetBreakdown> budgetBreakdown) {
        super();
        this.description = description;
        this.amount = amount;
        this.budgetBreakdown = budgetBreakdown;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("amount")
    public Amount getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(Amount amount) {
        this.amount = amount;
    }

    @JsonProperty("budgetBreakdown")
    public List<BudgetBreakdown> getBudgetBreakdown() {
        return budgetBreakdown;
    }

    @JsonProperty("budgetBreakdown")
    public void setBudgetBreakdown(List<BudgetBreakdown> budgetBreakdown) {
        this.budgetBreakdown = budgetBreakdown;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
