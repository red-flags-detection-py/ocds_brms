
package com.ocds.models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "sourceParty",
    "period",
    "measures",
    "classifications"
})
public class BudgetBreakdown {

    @JsonProperty("id")
    private String id;
    @JsonProperty("sourceParty")
    private SourceParty sourceParty;
    @JsonProperty("period")
    private Period period;
    @JsonProperty("measures")
    private Measures measures;
    @JsonProperty("classifications")
    private Classifications classifications;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public BudgetBreakdown() {
    }

    /**
     * 
     * @param classifications
     * @param period
     * @param measures
     * @param sourceParty
     * @param id
     */
    public BudgetBreakdown(String id, SourceParty sourceParty, Period period, Measures measures, Classifications classifications) {
        super();
        this.id = id;
        this.sourceParty = sourceParty;
        this.period = period;
        this.measures = measures;
        this.classifications = classifications;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("sourceParty")
    public SourceParty getSourceParty() {
        return sourceParty;
    }

    @JsonProperty("sourceParty")
    public void setSourceParty(SourceParty sourceParty) {
        this.sourceParty = sourceParty;
    }

    @JsonProperty("period")
    public Period getPeriod() {
        return period;
    }

    @JsonProperty("period")
    public void setPeriod(Period period) {
        this.period = period;
    }

    @JsonProperty("measures")
    public Measures getMeasures() {
        return measures;
    }

    @JsonProperty("measures")
    public void setMeasures(Measures measures) {
        this.measures = measures;
    }

    @JsonProperty("classifications")
    public Classifications getClassifications() {
        return classifications;
    }

    @JsonProperty("classifications")
    public void setClassifications(Classifications classifications) {
        this.classifications = classifications;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
