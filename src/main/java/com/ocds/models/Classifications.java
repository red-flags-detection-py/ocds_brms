
package com.ocds.models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "anio",
    "nivel",
    "entidad",
    "tipo_programa",
    "programa",
    "sub_programa",
    "proyecto",
    "financiador",
    "departamento",
    "objeto_gasto",
    "fuente_financiamiento",
    "cdp"
})
public class Classifications {

    @JsonProperty("anio")
    private String anio;
    @JsonProperty("nivel")
    private String nivel;
    @JsonProperty("entidad")
    private String entidad;
    @JsonProperty("tipo_programa")
    private String tipoPrograma;
    @JsonProperty("programa")
    private String programa;
    @JsonProperty("sub_programa")
    private String subPrograma;
    @JsonProperty("proyecto")
    private String proyecto;
    @JsonProperty("financiador")
    private String financiador;
    @JsonProperty("departamento")
    private String departamento;
    @JsonProperty("objeto_gasto")
    private String objetoGasto;
    @JsonProperty("fuente_financiamiento")
    private String fuenteFinanciamiento;
    @JsonProperty("cdp")
    private String cdp;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Classifications() {
    }

    /**
     * 
     * @param subPrograma
     * @param entidad
     * @param tipoPrograma
     * @param programa
     * @param proyecto
     * @param departamento
     * @param nivel
     * @param objetoGasto
     * @param anio
     * @param financiador
     * @param cdp
     * @param fuenteFinanciamiento
     */
    public Classifications(String anio, String nivel, String entidad, String tipoPrograma, String programa, String subPrograma, String proyecto, String financiador, String departamento, String objetoGasto, String fuenteFinanciamiento, String cdp) {
        super();
        this.anio = anio;
        this.nivel = nivel;
        this.entidad = entidad;
        this.tipoPrograma = tipoPrograma;
        this.programa = programa;
        this.subPrograma = subPrograma;
        this.proyecto = proyecto;
        this.financiador = financiador;
        this.departamento = departamento;
        this.objetoGasto = objetoGasto;
        this.fuenteFinanciamiento = fuenteFinanciamiento;
        this.cdp = cdp;
    }

    @JsonProperty("anio")
    public String getAnio() {
        return anio;
    }

    @JsonProperty("anio")
    public void setAnio(String anio) {
        this.anio = anio;
    }

    @JsonProperty("nivel")
    public String getNivel() {
        return nivel;
    }

    @JsonProperty("nivel")
    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    @JsonProperty("entidad")
    public String getEntidad() {
        return entidad;
    }

    @JsonProperty("entidad")
    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    @JsonProperty("tipo_programa")
    public String getTipoPrograma() {
        return tipoPrograma;
    }

    @JsonProperty("tipo_programa")
    public void setTipoPrograma(String tipoPrograma) {
        this.tipoPrograma = tipoPrograma;
    }

    @JsonProperty("programa")
    public String getPrograma() {
        return programa;
    }

    @JsonProperty("programa")
    public void setPrograma(String programa) {
        this.programa = programa;
    }

    @JsonProperty("sub_programa")
    public String getSubPrograma() {
        return subPrograma;
    }

    @JsonProperty("sub_programa")
    public void setSubPrograma(String subPrograma) {
        this.subPrograma = subPrograma;
    }

    @JsonProperty("proyecto")
    public String getProyecto() {
        return proyecto;
    }

    @JsonProperty("proyecto")
    public void setProyecto(String proyecto) {
        this.proyecto = proyecto;
    }

    @JsonProperty("financiador")
    public String getFinanciador() {
        return financiador;
    }

    @JsonProperty("financiador")
    public void setFinanciador(String financiador) {
        this.financiador = financiador;
    }

    @JsonProperty("departamento")
    public String getDepartamento() {
        return departamento;
    }

    @JsonProperty("departamento")
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    @JsonProperty("objeto_gasto")
    public String getObjetoGasto() {
        return objetoGasto;
    }

    @JsonProperty("objeto_gasto")
    public void setObjetoGasto(String objetoGasto) {
        this.objetoGasto = objetoGasto;
    }

    @JsonProperty("fuente_financiamiento")
    public String getFuenteFinanciamiento() {
        return fuenteFinanciamiento;
    }

    @JsonProperty("fuente_financiamiento")
    public void setFuenteFinanciamiento(String fuenteFinanciamiento) {
        this.fuenteFinanciamiento = fuenteFinanciamiento;
    }

    @JsonProperty("cdp")
    public String getCdp() {
        return cdp;
    }

    @JsonProperty("cdp")
    public void setCdp(String cdp) {
        this.cdp = cdp;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
