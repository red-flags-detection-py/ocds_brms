
package com.ocds.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "tag", "id", "date", "ocid", "awards", "parties", "contracts", "tender", "language",
		"initiationType", "buyer", "sources", "planning" })
public class CompiledRelease{

    @JsonProperty("tag")
    private List<String> tag = null;
    @JsonProperty("id")
    private String id;
    @JsonProperty("date")
    private String date;
    @JsonProperty("ocid")
    private String ocid;
    @JsonProperty("awards")
    private List<Award> awards = null;
    @JsonProperty("parties")
    private List<Party> parties = null;
    @JsonProperty("contracts")
    private List<Contract> contracts = null;
    @JsonProperty("tender")
    private Tender tender;
    @JsonProperty("language")
    private String language;
    @JsonProperty("initiationType")
    private String initiationType;
    @JsonProperty("buyer")
    private Buyer buyer;
    @JsonProperty("sources")
    private List<Source> sources = null;
    @JsonProperty("planning")
    private Planning planning;
    @JsonProperty("redFlags")
    private List<RedFlag> redFlags = null;
	@JsonProperty("complaints")
	private List<Complaint> complaints = null;
	@JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public CompiledRelease() {
    }

    /**
     *
     * @param date
     * @param tender
     * @param sources
     * @param language
     * @param contracts
     * @param buyer
     * @param initiationType
     * @param planning
     * @param awards
     * @param parties
     * @param tag
     * @param id
     * @param ocid
     */
    public CompiledRelease(List<String> tag, String id, String date, String ocid, List<Award> awards, List<Party> parties, List<Contract> contracts, Tender tender, String language, String initiationType, Buyer buyer, List<Source> sources, Planning planning, List<Complaint> complaints) {
        super();
        this.tag = tag;
        this.id = id;
        this.date = date;
        this.ocid = ocid;
        this.awards = awards;
        this.parties = parties;
        this.contracts = contracts;
        this.tender = tender;
        this.language = language;
        this.initiationType = initiationType;
        this.buyer = buyer;
        this.sources = sources;
        this.planning = planning;
        this.complaints = complaints;
    }

    public void addRedFlag(String title, String message){
        if(this.redFlags == null) {
            this.redFlags = new ArrayList<>();
        }
        RedFlag item = new RedFlag(title, message);
        if(!this.redFlags.contains(item)){
            System.out.println("--> " + title + " --> "+ message);
            this.redFlags.add(item);
        }
    }

    public List<RedFlag> getRedFlags() {
        return redFlags;
    }

    public void setRedFlags(List<RedFlag> redFlags) {
        this.redFlags = redFlags;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("tag")
    public List<String> getTag() {
        return tag;
    }

    @JsonProperty("tag")
    public void setTag(List<String> tag) {
        this.tag = tag;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("ocid")
    public String getOcid() {
        return ocid;
    }

    @JsonProperty("ocid")
    public void setOcid(String ocid) {
        this.ocid = ocid;
    }

    @JsonProperty("awards")
    public List<Award> getAwards() {
        return awards;
    }

    @JsonProperty("awards")
    public void setAwards(List<Award> awards) {
        this.awards = awards;
    }

    @JsonProperty("parties")
    public List<Party> getParties() {
        return parties;
    }

    @JsonProperty("parties")
    public void setParties(List<Party> parties) {
        this.parties = parties;
    }

    @JsonProperty("contracts")
    public List<Contract> getContracts() {
        return contracts;
    }

    @JsonProperty("contracts")
    public void setContracts(List<Contract> contracts) {
        this.contracts = contracts;
    }

    @JsonProperty("tender")
    public Tender getTender() {
        return tender;
    }

    @JsonProperty("tender")
    public void setTender(Tender tender) {
        this.tender = tender;
    }

    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(String language) {
        this.language = language;
    }

    @JsonProperty("initiationType")
    public String getInitiationType() {
        return initiationType;
    }

    @JsonProperty("initiationType")
    public void setInitiationType(String initiationType) {
        this.initiationType = initiationType;
    }

    @JsonProperty("buyer")
    public Buyer getBuyer() {
        return buyer;
    }

    @JsonProperty("buyer")
    public void setBuyer(Buyer buyer) {
        this.buyer = buyer;
    }

    @JsonProperty("sources")
    public List<Source> getSources() {
        return sources;
    }

    @JsonProperty("sources")
    public void setSources(List<Source> sources) {
        this.sources = sources;
    }

    @JsonProperty("planning")
    public Planning getPlanning() {
        return planning;
    }

    @JsonProperty("planning")
    public void setPlanning(Planning planning) {
        this.planning = planning;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }


	@JsonProperty("complaints")
	public List<Complaint> getComplaints() {
		return complaints;
	}

	@JsonProperty("complaints")
	public void setComplaints(List<Complaint> complaints) {
		this.complaints = complaints;
	}

	@JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }



}
