
package com.ocds.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "guarantees", "implementation", "awardID", "status", "statusDetails", "dateSigned", "period",
		"value", "documents" })
public class Contract {

	@JsonProperty("id")
	private String id;
	@JsonProperty("guarantees")
	private List<Guarantee> guarantees = null;
	@JsonProperty("implementation")
	private Implementation implementation;
	@JsonProperty("awardID")
	private String awardID;
	@JsonProperty("status")
	private String status;
	@JsonProperty("statusDetails")
	private String statusDetails;
	@JsonProperty("dateSigned")
	private String dateSigned;
	@JsonProperty("period")
	private Period period;
	@JsonProperty("value")
	private Value value;
	@JsonProperty("documents")
	private List<Document> documents = null;
	@JsonProperty("amendments")
	private List<Amendment> amendments = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Contract() {
	}

	/**
	 * 
	 * @param awardID
	 * @param period
	 * @param documents
	 * @param implementation
	 * @param statusDetails
	 * @param id
	 * @param dateSigned
	 * @param guarantees
	 * @param value
	 * @param status
	 */
	public Contract(String id, List<Guarantee> guarantees, Implementation implementation, String awardID, String status,
			String statusDetails, String dateSigned, Period period, Value value, List<Document> documents) {
		super();
		this.id = id;
		this.guarantees = guarantees;
		this.implementation = implementation;
		this.awardID = awardID;
		this.status = status;
		this.statusDetails = statusDetails;
		this.dateSigned = dateSigned;
		this.period = period;
		this.value = value;
		this.documents = documents;
	}

	public Contract(String id, List<Guarantee> guarantees, Implementation implementation, String awardID, String status,
			String statusDetails, String dateSigned, Period period, Value value, List<Document> documents,
			List<Amendment> amendments, Map<String, Object> additionalProperties) {
		super();
		this.id = id;
		this.guarantees = guarantees;
		this.implementation = implementation;
		this.awardID = awardID;
		this.status = status;
		this.statusDetails = statusDetails;
		this.dateSigned = dateSigned;
		this.period = period;
		this.value = value;
		this.documents = documents;
		this.amendments = amendments;
		this.additionalProperties = additionalProperties;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("guarantees")
	public List<Guarantee> getGuarantees() {
		return guarantees;
	}

	@JsonProperty("guarantees")
	public void setGuarantees(List<Guarantee> guarantees) {
		this.guarantees = guarantees;
	}

	@JsonProperty("implementation")
	public Implementation getImplementation() {
		return implementation;
	}

	@JsonProperty("implementation")
	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	@JsonProperty("awardID")
	public String getAwardID() {
		return awardID;
	}

	@JsonProperty("awardID")
	public void setAwardID(String awardID) {
		this.awardID = awardID;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("statusDetails")
	public String getStatusDetails() {
		return statusDetails;
	}

	@JsonProperty("statusDetails")
	public void setStatusDetails(String statusDetails) {
		this.statusDetails = statusDetails;
	}

	@JsonProperty("dateSigned")
	public String getDateSigned() {
		return dateSigned;
	}

	@JsonProperty("dateSigned")
	public void setDateSigned(String dateSigned) {
		this.dateSigned = dateSigned;
	}

	@JsonProperty("period")
	public Period getPeriod() {
		return period;
	}

	@JsonProperty("period")
	public void setPeriod(Period period) {
		this.period = period;
	}

	@JsonProperty("value")
	public Value getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(Value value) {
		this.value = value;
	}

	@JsonProperty("documents")
	public List<Document> getDocuments() {
		return documents;
	}

	@JsonProperty("documents")
	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	@JsonProperty("amendments")
	public List<Amendment> getAmendments() {
		return amendments;
	}

	@JsonProperty("amendments")
	public void setAmendments(List<Amendment> amendments) {
		this.amendments = amendments;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
