
package com.ocds.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "level",
    "entityType",
    "type",
    "legalEntityTypeDetail",
    "activityTypes",
    "categories"
})
public class Details {

    @JsonProperty("level")
    private String level;
    @JsonProperty("entityType")
    private String entityType;
    @JsonProperty("type")
    private String type;
    @JsonProperty("legalEntityTypeDetail")
    private String legalEntityTypeDetail;
    @JsonProperty("activityTypes")
    private List<String> activityTypes = null;
    @JsonProperty("categories")
    private List<Category> categories = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Details() {
    }

    /**
     * 
     * @param legalEntityTypeDetail
     * @param level
     * @param entityType
     * @param categories
     * @param type
     * @param activityTypes
     */
    public Details(String level, String entityType, String type, String legalEntityTypeDetail, List<String> activityTypes, List<Category> categories) {
        super();
        this.level = level;
        this.entityType = entityType;
        this.type = type;
        this.legalEntityTypeDetail = legalEntityTypeDetail;
        this.activityTypes = activityTypes;
        this.categories = categories;
    }

    @JsonProperty("level")
    public String getLevel() {
        return level;
    }

    @JsonProperty("level")
    public void setLevel(String level) {
        this.level = level;
    }

    @JsonProperty("entityType")
    public String getEntityType() {
        return entityType;
    }

    @JsonProperty("entityType")
    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("legalEntityTypeDetail")
    public String getLegalEntityTypeDetail() {
        return legalEntityTypeDetail;
    }

    @JsonProperty("legalEntityTypeDetail")
    public void setLegalEntityTypeDetail(String legalEntityTypeDetail) {
        this.legalEntityTypeDetail = legalEntityTypeDetail;
    }

    @JsonProperty("activityTypes")
    public List<String> getActivityTypes() {
        return activityTypes;
    }

    @JsonProperty("activityTypes")
    public void setActivityTypes(List<String> activityTypes) {
        this.activityTypes = activityTypes;
    }

    @JsonProperty("categories")
    public List<Category> getCategories() {
        return categories;
    }

    @JsonProperty("categories")
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
