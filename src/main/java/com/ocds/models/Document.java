
package com.ocds.models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "documentTypeDetails",
    "url",
    "datePublished",
    "language",
    "title",
    "format"
})
public class Document {

    @JsonProperty("id")
    private String id;
    @JsonProperty("documentTypeDetails")
    private String documentTypeDetails;
    @JsonProperty("url")
    private String url;
    @JsonProperty("datePublished")
    private String datePublished;
    @JsonProperty("language")
    private String language;
    @JsonProperty("title")
    private String title;
    @JsonProperty("format")
    private String format;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Document() {
    }

    /**
     * 
     * @param datePublished
     * @param documentTypeDetails
     * @param format
     * @param language
     * @param id
     * @param title
     * @param url
     */
    public Document(String id, String documentTypeDetails, String url, String datePublished, String language, String title, String format) {
        super();
        this.id = id;
        this.documentTypeDetails = documentTypeDetails;
        this.url = url;
        this.datePublished = datePublished;
        this.language = language;
        this.title = title;
        this.format = format;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("documentTypeDetails")
    public String getDocumentTypeDetails() {
        return documentTypeDetails;
    }

    @JsonProperty("documentTypeDetails")
    public void setDocumentTypeDetails(String documentTypeDetails) {
        this.documentTypeDetails = documentTypeDetails;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("datePublished")
    public String getDatePublished() {
        return datePublished;
    }

    @JsonProperty("datePublished")
    public void setDatePublished(String datePublished) {
        this.datePublished = datePublished;
    }

    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(String language) {
        this.language = language;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("format")
    public String getFormat() {
        return format;
    }

    @JsonProperty("format")
    public void setFormat(String format) {
        this.format = format;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
