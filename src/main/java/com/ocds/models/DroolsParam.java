package com.ocds.models;

import java.util.List;

public class DroolsParam {
    List<Result> results;
    Result result;

    public DroolsParam() {
    }

    public DroolsParam(List<Result> results, Result result) {
        this.results = results;
        this.result = result;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
