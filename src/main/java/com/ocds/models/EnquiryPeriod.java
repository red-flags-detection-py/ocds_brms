
package com.ocds.models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "endDate",
    "startDate",
    "durationInDays"
})
public class EnquiryPeriod {

    @JsonProperty("endDate")
    private String endDate;
    @JsonProperty("startDate")
    private String startDate;
    @JsonProperty("durationInDays")
    private Integer durationInDays;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public EnquiryPeriod() {
    }

    /**
     * 
     * @param durationInDays
     * @param endDate
     * @param startDate
     */
    public EnquiryPeriod(String endDate, String startDate, Integer durationInDays) {
        super();
        this.endDate = endDate;
        this.startDate = startDate;
        this.durationInDays = durationInDays;
    }

    @JsonProperty("endDate")
    public String getEndDate() {
        return endDate;
    }

    @JsonProperty("endDate")
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @JsonProperty("startDate")
    public String getStartDate() {
        return startDate;
    }

    @JsonProperty("startDate")
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    @JsonProperty("durationInDays")
    public Integer getDurationInDays() {
        return durationInDays;
    }

    @JsonProperty("durationInDays")
    public void setDurationInDays(Integer durationInDays) {
        this.durationInDays = durationInDays;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
