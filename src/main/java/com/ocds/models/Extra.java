
package com.ocds.models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "buyerFullName",
        "parentTop",
        "lastSection",
        "daysTenderedPeriod",
        "daysTenderedPublished",
        "has_md5"
})
public class Extra {

    @JsonProperty("buyerFullName")
    private String buyerFullName;
    @JsonProperty("parentTop")
    private ParentTop parentTop;
    @JsonProperty("lastSection")
    private String lastSection;
    @JsonProperty("daysTenderedPeriod")
    private String daysTenderedPeriod;
    @JsonProperty("daysTenderedPublished")
    private String daysTenderedPublished;
    @JsonProperty("has_md5")
    private String has_md5;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Extra() {
    }

    public Extra(String buyerFullName, ParentTop parentTop, String lastSection, String daysTenderedPeriod, String daysTenderedPublished, String has_md5) {
        this.buyerFullName = buyerFullName;
        this.parentTop = parentTop;
        this.lastSection = lastSection;
        this.daysTenderedPeriod = daysTenderedPeriod;
        this.daysTenderedPublished = daysTenderedPublished;
        this.has_md5 = has_md5;
    }
    @JsonProperty("buyerFullName")
    public String getBuyerFullName() {
        return buyerFullName;
    }
    @JsonProperty("buyerFullName")
    public void setBuyerFullName(String buyerFullName) {
        this.buyerFullName = buyerFullName;
    }
    @JsonProperty("parentTop")
    public ParentTop getParentTop() {
        return parentTop;
    }
    @JsonProperty("parentTop")
    public void setParentTop(ParentTop parentTop) {
        this.parentTop = parentTop;
    }
    @JsonProperty("lastSection")
    public String getLastSection() {
        return lastSection;
    }
    @JsonProperty("lastSection")
    public void setLastSection(String lastSection) {
        this.lastSection = lastSection;
    }
    @JsonProperty("daysTenderedPeriod")
    public String getDaysTenderedPeriod() {
        return daysTenderedPeriod;
    }
    @JsonProperty("daysTenderedPeriod")
    public void setDaysTenderedPeriod(String daysTenderedPeriod) {
        this.daysTenderedPeriod = daysTenderedPeriod;
    }
    @JsonProperty("daysTenderedPublished")
    public String getDaysTenderedPublished() {
        return daysTenderedPublished;
    }
    @JsonProperty("daysTenderedPublished")
    public void setDaysTenderedPublished(String daysTenderedPublished) {
        this.daysTenderedPublished = daysTenderedPublished;
    }
    @JsonProperty("has_md5")
    public String getHas_md5() {
        return has_md5;
    }
    @JsonProperty("has_md5")
    public void setHas_md5(String has_md5) {
        this.has_md5 = has_md5;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
