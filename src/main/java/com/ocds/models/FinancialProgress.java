
package com.ocds.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "breakdown"
})
public class FinancialProgress {

    @JsonProperty("breakdown")
    private List<Breakdown> breakdown = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public FinancialProgress() {
    }

    /**
     * 
     * @param breakdown
     */
    public FinancialProgress(List<Breakdown> breakdown) {
        super();
        this.breakdown = breakdown;
    }

    @JsonProperty("breakdown")
    public List<Breakdown> getBreakdown() {
        return breakdown;
    }

    @JsonProperty("breakdown")
    public void setBreakdown(List<Breakdown> breakdown) {
        this.breakdown = breakdown;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
