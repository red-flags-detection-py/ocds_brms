
package com.ocds.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "bill",
    "retentions"
})
public class FinantialObligation {

    @JsonProperty("id")
    private String id;
    @JsonProperty("bill")
    private Bill bill;
    @JsonProperty("retentions")
    private List<Retention> retentions = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public FinantialObligation() {
    }

    /**
     * 
     * @param bill
     * @param id
     * @param retentions
     */
    public FinantialObligation(String id, Bill bill, List<Retention> retentions) {
        super();
        this.id = id;
        this.bill = bill;
        this.retentions = retentions;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("bill")
    public Bill getBill() {
        return bill;
    }

    @JsonProperty("bill")
    public void setBill(Bill bill) {
        this.bill = bill;
    }

    @JsonProperty("retentions")
    public List<Retention> getRetentions() {
        return retentions;
    }

    @JsonProperty("retentions")
    public void setRetentions(List<Retention> retentions) {
        this.retentions = retentions;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
