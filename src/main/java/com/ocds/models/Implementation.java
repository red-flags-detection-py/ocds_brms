
package com.ocds.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "milestones",
    "purchaseOrders",
    "financialProgress",
    "transactions"
})
public class Implementation {

    @JsonProperty("milestones")
    private List<Milestone> milestones = null;
    @JsonProperty("purchaseOrders")
    private List<PurchaseOrder> purchaseOrders = null;
    @JsonProperty("financialProgress")
    private FinancialProgress financialProgress;
    @JsonProperty("transactions")
    private List<Transaction> transactions = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Implementation() {
    }

    /**
     * 
     * @param financialProgress
     * @param milestones
     * @param transactions
     * @param purchaseOrders
     */
    public Implementation(List<Milestone> milestones, List<PurchaseOrder> purchaseOrders, FinancialProgress financialProgress, List<Transaction> transactions) {
        super();
        this.milestones = milestones;
        this.purchaseOrders = purchaseOrders;
        this.financialProgress = financialProgress;
        this.transactions = transactions;
    }

    @JsonProperty("milestones")
    public List<Milestone> getMilestones() {
        return milestones;
    }

    @JsonProperty("milestones")
    public void setMilestones(List<Milestone> milestones) {
        this.milestones = milestones;
    }

    @JsonProperty("purchaseOrders")
    public List<PurchaseOrder> getPurchaseOrders() {
        return purchaseOrders;
    }

    @JsonProperty("purchaseOrders")
    public void setPurchaseOrders(List<PurchaseOrder> purchaseOrders) {
        this.purchaseOrders = purchaseOrders;
    }

    @JsonProperty("financialProgress")
    public FinancialProgress getFinancialProgress() {
        return financialProgress;
    }

    @JsonProperty("financialProgress")
    public void setFinancialProgress(FinancialProgress financialProgress) {
        this.financialProgress = financialProgress;
    }

    @JsonProperty("transactions")
    public List<Transaction> getTransactions() {
        return transactions;
    }

    @JsonProperty("transactions")
    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
