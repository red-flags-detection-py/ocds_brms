
package com.ocds.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "description",
    "classification",
    "additionalClassifications",
    "quantity",
    "unit",
    "relatedLot",
    "attributes"
})
public class Item {

    @JsonProperty("id")
    private String id;
    @JsonProperty("description")
    private String description;
    @JsonProperty("classification")
    private Classification classification;
    @JsonProperty("additionalClassifications")
    private List<AdditionalClassification> additionalClassifications = null;
    @JsonProperty("quantity")
    private Long quantity;
    @JsonProperty("unit")
    private Unit unit;
    @JsonProperty("relatedLot")
    private String relatedLot;
    @JsonProperty("attributes")
    private List<Attribute> attributes = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Item() {
    }

    /**
     * 
     * @param additionalClassifications
     * @param unit
     * @param quantity
     * @param description
     * @param relatedLot
     * @param attributes
     * @param id
     * @param classification
     */
    public Item(String id, String description, Classification classification, List<AdditionalClassification> additionalClassifications, Long quantity, Unit unit, String relatedLot, List<Attribute> attributes) {
        super();
        this.id = id;
        this.description = description;
        this.classification = classification;
        this.additionalClassifications = additionalClassifications;
        this.quantity = quantity;
        this.unit = unit;
        this.relatedLot = relatedLot;
        this.attributes = attributes;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("classification")
    public Classification getClassification() {
        return classification;
    }

    @JsonProperty("classification")
    public void setClassification(Classification classification) {
        this.classification = classification;
    }

    @JsonProperty("additionalClassifications")
    public List<AdditionalClassification> getAdditionalClassifications() {
        return additionalClassifications;
    }

    @JsonProperty("additionalClassifications")
    public void setAdditionalClassifications(List<AdditionalClassification> additionalClassifications) {
        this.additionalClassifications = additionalClassifications;
    }

    @JsonProperty("quantity")
    public Long getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("unit")
    public Unit getUnit() {
        return unit;
    }

    @JsonProperty("unit")
    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    @JsonProperty("relatedLot")
    public String getRelatedLot() {
        return relatedLot;
    }

    @JsonProperty("relatedLot")
    public void setRelatedLot(String relatedLot) {
        this.relatedLot = relatedLot;
    }

    @JsonProperty("attributes")
    public List<Attribute> getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
