
package com.ocds.models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "title",
    "status",
    "statusDetails",
    "value",
    "simultaneousSupply"
})
public class Lot {

    @JsonProperty("id")
    private String id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("status")
    private String status;
    @JsonProperty("statusDetails")
    private String statusDetails;
    @JsonProperty("value")
    private Value value;
    @JsonProperty("simultaneousSupply")
    private Boolean simultaneousSupply;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Lot() {
    }

    /**
     * 
     * @param simultaneousSupply
     * @param statusDetails
     * @param id
     * @param title
     * @param value
     * @param status
     */
    public Lot(String id, String title, String status, String statusDetails, Value value, Boolean simultaneousSupply) {
        super();
        this.id = id;
        this.title = title;
        this.status = status;
        this.statusDetails = statusDetails;
        this.value = value;
        this.simultaneousSupply = simultaneousSupply;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("statusDetails")
    public String getStatusDetails() {
        return statusDetails;
    }

    @JsonProperty("statusDetails")
    public void setStatusDetails(String statusDetails) {
        this.statusDetails = statusDetails;
    }

    @JsonProperty("value")
    public Value getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(Value value) {
        this.value = value;
    }

    @JsonProperty("simultaneousSupply")
    public Boolean getSimultaneousSupply() {
        return simultaneousSupply;
    }

    @JsonProperty("simultaneousSupply")
    public void setSimultaneousSupply(Boolean simultaneousSupply) {
        this.simultaneousSupply = simultaneousSupply;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
