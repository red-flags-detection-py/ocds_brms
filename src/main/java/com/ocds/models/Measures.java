
package com.ocds.models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "monto_utilizado",
    "monto_a_utilizar"
})
public class Measures {

    @JsonProperty("monto_utilizado")
    private String montoUtilizado;
    @JsonProperty("monto_a_utilizar")
    private String montoAUtilizar;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Measures() {
    }

    /**
     * 
     * @param montoUtilizado
     * @param montoAUtilizar
     */
    public Measures(String montoUtilizado, String montoAUtilizar) {
        super();
        this.montoUtilizado = montoUtilizado;
        this.montoAUtilizar = montoAUtilizar;
    }

    @JsonProperty("monto_utilizado")
    public String getMontoUtilizado() {
        return montoUtilizado;
    }

    @JsonProperty("monto_utilizado")
    public void setMontoUtilizado(String montoUtilizado) {
        this.montoUtilizado = montoUtilizado;
    }

    @JsonProperty("monto_a_utilizar")
    public String getMontoAUtilizar() {
        return montoAUtilizar;
    }

    @JsonProperty("monto_a_utilizar")
    public void setMontoAUtilizar(String montoAUtilizar) {
        this.montoAUtilizar = montoAUtilizar;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
