
package com.ocds.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "identifier",
    "additionalIdentifiers",
    "contactPoint",
    "address",
    "roles",
    "details",
    "memberOf"
})
public class Party {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("identifier")
    private Identifier identifier;
    @JsonProperty("additionalIdentifiers")
    private List<AdditionalIdentifier> additionalIdentifiers = null;
    @JsonProperty("contactPoint")
    private ContactPoint contactPoint;
    @JsonProperty("address")
    private Address address;
    @JsonProperty("roles")
    private List<String> roles = null;
    @JsonProperty("details")
    private Details details;
    @JsonProperty("memberOf")
    private List<MemberOf> memberOf = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Party() {
    }

    /**
     * 
     * @param identifier
     * @param address
     * @param contactPoint
     * @param roles
     * @param name
     * @param additionalIdentifiers
     * @param details
     * @param id
     * @param memberOf
     */
    public Party(String id, String name, Identifier identifier, List<AdditionalIdentifier> additionalIdentifiers, ContactPoint contactPoint, Address address, List<String> roles, Details details, List<MemberOf> memberOf) {
        super();
        this.id = id;
        this.name = name;
        this.identifier = identifier;
        this.additionalIdentifiers = additionalIdentifiers;
        this.contactPoint = contactPoint;
        this.address = address;
        this.roles = roles;
        this.details = details;
        this.memberOf = memberOf;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("identifier")
    public Identifier getIdentifier() {
        return identifier;
    }

    @JsonProperty("identifier")
    public void setIdentifier(Identifier identifier) {
        this.identifier = identifier;
    }

    @JsonProperty("additionalIdentifiers")
    public List<AdditionalIdentifier> getAdditionalIdentifiers() {
        return additionalIdentifiers;
    }

    @JsonProperty("additionalIdentifiers")
    public void setAdditionalIdentifiers(List<AdditionalIdentifier> additionalIdentifiers) {
        this.additionalIdentifiers = additionalIdentifiers;
    }

    @JsonProperty("contactPoint")
    public ContactPoint getContactPoint() {
        return contactPoint;
    }

    @JsonProperty("contactPoint")
    public void setContactPoint(ContactPoint contactPoint) {
        this.contactPoint = contactPoint;
    }

    @JsonProperty("address")
    public Address getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(Address address) {
        this.address = address;
    }

    @JsonProperty("roles")
    public List<String> getRoles() {
        return roles;
    }

    @JsonProperty("roles")
    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    @JsonProperty("details")
    public Details getDetails() {
        return details;
    }

    @JsonProperty("details")
    public void setDetails(Details details) {
        this.details = details;
    }

    @JsonProperty("memberOf")
    public List<MemberOf> getMemberOf() {
        return memberOf;
    }

    @JsonProperty("memberOf")
    public void setMemberOf(List<MemberOf> memberOf) {
        this.memberOf = memberOf;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
