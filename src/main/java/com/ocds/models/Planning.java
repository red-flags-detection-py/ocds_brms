
package com.ocds.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "identifier",
    "estimatedDate",
    "budget",
    "items"
})
public class Planning {

    @JsonProperty("identifier")
    private String identifier;
    @JsonProperty("estimatedDate")
    private String estimatedDate;
    @JsonProperty("budget")
    private Budget budget;
    @JsonProperty("items")
    private List<Item> items = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Planning() {
    }

    /**
     * 
     * @param identifier
     * @param estimatedDate
     * @param items
     * @param budget
     */
    public Planning(String identifier, String estimatedDate, Budget budget, List<Item> items) {
        super();
        this.identifier = identifier;
        this.estimatedDate = estimatedDate;
        this.budget = budget;
        this.items = items;
    }

    @JsonProperty("identifier")
    public String getIdentifier() {
        return identifier;
    }

    @JsonProperty("identifier")
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @JsonProperty("estimatedDate")
    public String getEstimatedDate() {
        return estimatedDate;
    }

    @JsonProperty("estimatedDate")
    public void setEstimatedDate(String estimatedDate) {
        this.estimatedDate = estimatedDate;
    }

    @JsonProperty("budget")
    public Budget getBudget() {
        return budget;
    }

    @JsonProperty("budget")
    public void setBudget(Budget budget) {
        this.budget = budget;
    }

    @JsonProperty("items")
    public List<Item> getItems() {
        return items;
    }

    @JsonProperty("items")
    public void setItems(List<Item> items) {
        this.items = items;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
