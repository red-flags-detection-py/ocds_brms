
package com.ocds.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "uri",
    "publishedDate",
    "publisher",
    "license",
    "publicationPolicy",
    "version",
    "extensions",
    "records",
    "pagination"
})
public class Process {

    @JsonProperty("uri")
    private String uri;
    @JsonProperty("publishedDate")
    private String publishedDate;
    @JsonProperty("publisher")
    private Publisher publisher;
    @JsonProperty("license")
    private String license;
    @JsonProperty("publicationPolicy")
    private String publicationPolicy;
    @JsonProperty("version")
    private String version;
    @JsonProperty("extensions")
    private List<String> extensions = null;
    @JsonProperty("records")
    private List<Record> records = null;
    @JsonProperty("pagination")
    private Pagination pagination;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Process() {
    }

    /**
     * 
     * @param publicationPolicy
     * @param license
     * @param extensions
     * @param pagination
     * @param records
     * @param publisher
     * @param publishedDate
     * @param uri
     * @param version
     */
    public Process(String uri, String publishedDate, Publisher publisher, String license, String publicationPolicy, String version, List<String> extensions, List<Record> records, Pagination pagination) {
        super();
        this.uri = uri;
        this.publishedDate = publishedDate;
        this.publisher = publisher;
        this.license = license;
        this.publicationPolicy = publicationPolicy;
        this.version = version;
        this.extensions = extensions;
        this.records = records;
        this.pagination = pagination;
    }

    @JsonProperty("uri")
    public String getUri() {
        return uri;
    }

    @JsonProperty("uri")
    public void setUri(String uri) {
        this.uri = uri;
    }

    @JsonProperty("publishedDate")
    public String getPublishedDate() {
        return publishedDate;
    }

    @JsonProperty("publishedDate")
    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    @JsonProperty("publisher")
    public Publisher getPublisher() {
        return publisher;
    }

    @JsonProperty("publisher")
    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    @JsonProperty("license")
    public String getLicense() {
        return license;
    }

    @JsonProperty("license")
    public void setLicense(String license) {
        this.license = license;
    }

    @JsonProperty("publicationPolicy")
    public String getPublicationPolicy() {
        return publicationPolicy;
    }

    @JsonProperty("publicationPolicy")
    public void setPublicationPolicy(String publicationPolicy) {
        this.publicationPolicy = publicationPolicy;
    }

    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    @JsonProperty("extensions")
    public List<String> getExtensions() {
        return extensions;
    }

    @JsonProperty("extensions")
    public void setExtensions(List<String> extensions) {
        this.extensions = extensions;
    }

    @JsonProperty("records")
    public List<Record> getRecords() {
        return records;
    }

    @JsonProperty("records")
    public void setRecords(List<Record> records) {
        this.records = records;
    }

    @JsonProperty("pagination")
    public Pagination getPagination() {
        return pagination;
    }

    @JsonProperty("pagination")
    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
