
package com.ocds.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ocid",
    "releases",
    "compiledRelease"
})
public class Record {

    @JsonProperty("ocid")
    private String ocid;
    @JsonProperty("releases")
    private List<Release> releases = null;
    @JsonProperty("compiledRelease")
    private CompiledRelease compiledRelease;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Record() {
    }

    /**
     * 
     * @param compiledRelease
     * @param ocid
     * @param releases
     */
    public Record(String ocid, List<Release> releases, CompiledRelease compiledRelease) {
        super();
        this.ocid = ocid;
        this.releases = releases;
        this.compiledRelease = compiledRelease;
    }

    @JsonProperty("ocid")
    public String getOcid() {
        return ocid;
    }

    @JsonProperty("ocid")
    public void setOcid(String ocid) {
        this.ocid = ocid;
    }

    @JsonProperty("releases")
    public List<Release> getReleases() {
        return releases;
    }

    @JsonProperty("releases")
    public void setReleases(List<Release> releases) {
        this.releases = releases;
    }

    @JsonProperty("compiledRelease")
    public CompiledRelease getCompiledRelease() {
        return compiledRelease;
    }

    @JsonProperty("compiledRelease")
    public void setCompiledRelease(CompiledRelease compiledRelease) {
        this.compiledRelease = compiledRelease;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
