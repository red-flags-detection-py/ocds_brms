
package com.ocds.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "doc",
        "extra",
})
public class Result {

    @JsonProperty("doc")
    private Doc doc;
    @JsonProperty("extra")
    private Extra extra;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Result() {
    }

    public Result(Doc doc, Extra extra) {
        this.doc = doc;
        this.extra = extra;
    }

    @JsonProperty("doc")
    public Doc getDoc() {
        return doc;
    }
    @JsonProperty("doc")
    public void setDoc(Doc doc) {
        this.doc = doc;
    }

    @JsonProperty("extra")
    public Extra getExtra() {
        return extra;
    }
    @JsonProperty("extra")
    public void setExtra(Extra extra) {
        this.extra = extra;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
