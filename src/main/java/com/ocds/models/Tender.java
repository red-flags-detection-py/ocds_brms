
package com.ocds.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "title", "status", "awardCriteria", "awardCriteriaDetails", "submissionMethod", "bidOpening",
		"submissionMethodDetails", "eligibilityCriteria", "statusDetails", "enquiriesAddress",
		"mainProcurementCategoryDetails", "hasEnquiries", "value", "datePublished", "tenderPeriod", "awardPeriod",
		"enquiryPeriod", "mainProcurementCategory", "additionalProcurementCategories", "procurementMethod",
		"procurementMethodDetails", "procuringEntity", "criteria", "lots", "notifiedSuppliers", "items", "tenderers",
		"documents", "numberOfTenderers" })
public class Tender {

	@JsonProperty("id")
	private String id;
	@JsonProperty("title")
	private String title;
	@JsonProperty("status")
	private String status;
	@JsonProperty("awardCriteria")
	private String awardCriteria;
	@JsonProperty("awardCriteriaDetails")
	private String awardCriteriaDetails;
	@JsonProperty("submissionMethod")
	private List<String> submissionMethod = null;
	@JsonProperty("bidOpening")
	private BidOpening bidOpening;
	@JsonProperty("submissionMethodDetails")
	private String submissionMethodDetails;
	@JsonProperty("eligibilityCriteria")
	private String eligibilityCriteria;
	@JsonProperty("statusDetails")
	private String statusDetails;
	@JsonProperty("enquiriesAddress")
	private EnquiriesAddress enquiriesAddress;
	@JsonProperty("mainProcurementCategoryDetails")
	private String mainProcurementCategoryDetails;
	@JsonProperty("hasEnquiries")
	private Boolean hasEnquiries;
	@JsonProperty("enquiries")
	private List<Enquiry> enquiries = null;
	@JsonProperty("value")
	private Value value;
	@JsonProperty("datePublished")
	private String datePublished;
	@JsonProperty("tenderPeriod")
	private TenderPeriod tenderPeriod;
	@JsonProperty("awardPeriod")
	private AwardPeriod awardPeriod;
	@JsonProperty("enquiryPeriod")
	private EnquiryPeriod enquiryPeriod;
	@JsonProperty("mainProcurementCategory")
	private String mainProcurementCategory;
	@JsonProperty("additionalProcurementCategories")
	private List<String> additionalProcurementCategories = null;
	@JsonProperty("procurementMethod")
	private String procurementMethod;
	@JsonProperty("procurementMethodDetails")
	private String procurementMethodDetails;
	@JsonProperty("procuringEntity")
	private ProcuringEntity procuringEntity;
	@JsonProperty("criteria")
	private List<Criterium> criteria = null;
	@JsonProperty("lots")
	private List<Lot> lots = null;
	@JsonProperty("notifiedSuppliers")
	private List<NotifiedSupplier> notifiedSuppliers = null;
	@JsonProperty("items")
	private List<Item> items = null;
	@JsonProperty("tenderers")
	private List<Tenderer> tenderers = null;
	@JsonProperty("documents")
	private List<Document> documents = null;
	@JsonProperty("numberOfTenderers")
	private Integer numberOfTenderers;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Tender() {
	}

	/**
	 * 
	 * @param mainProcurementCategoryDetails
	 * @param documents
	 * @param criteria
	 * @param awardPeriod
	 * @param bidOpening
	 * @param mainProcurementCategory
	 * @param title
	 * @param procurementMethodDetails
	 * @param additionalProcurementCategories
	 * @param enquiryPeriod
	 * @param procurementMethod
	 * @param awardCriteria
	 * @param eligibilityCriteria
	 * @param id
	 * @param value
	 * @param tenderPeriod
	 * @param notifiedSuppliers
	 * @param procuringEntity
	 * @param submissionMethod
	 * @param hasEnquiries
	 * @param datePublished
	 * @param lots
	 * @param numberOfTenderers
	 * @param enquiriesAddress
	 * @param submissionMethodDetails
	 * @param statusDetails
	 * @param awardCriteriaDetails
	 * @param items
	 * @param tenderers
	 * @param status
	 */

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public Tender(String id, String title, String status, String awardCriteria, String awardCriteriaDetails,
			List<String> submissionMethod, BidOpening bidOpening, String submissionMethodDetails,
			String eligibilityCriteria, String statusDetails, EnquiriesAddress enquiriesAddress,
			String mainProcurementCategoryDetails, Boolean hasEnquiries, List<Enquiry> enquiries, Value value,
			String datePublished, TenderPeriod tenderPeriod, AwardPeriod awardPeriod, EnquiryPeriod enquiryPeriod,
			String mainProcurementCategory, List<String> additionalProcurementCategories, String procurementMethod,
			String procurementMethodDetails, ProcuringEntity procuringEntity, List<Criterium> criteria, List<Lot> lots,
			List<NotifiedSupplier> notifiedSuppliers, List<Item> items, List<Tenderer> tenderers,
			List<Document> documents, Integer numberOfTenderers) {
		super();
		this.id = id;
		this.title = title;
		this.status = status;
		this.awardCriteria = awardCriteria;
		this.awardCriteriaDetails = awardCriteriaDetails;
		this.submissionMethod = submissionMethod;
		this.bidOpening = bidOpening;
		this.submissionMethodDetails = submissionMethodDetails;
		this.eligibilityCriteria = eligibilityCriteria;
		this.statusDetails = statusDetails;
		this.enquiriesAddress = enquiriesAddress;
		this.mainProcurementCategoryDetails = mainProcurementCategoryDetails;
		this.hasEnquiries = hasEnquiries;
		this.enquiries = enquiries;
		this.value = value;
		this.datePublished = datePublished;
		this.tenderPeriod = tenderPeriod;
		this.awardPeriod = awardPeriod;
		this.enquiryPeriod = enquiryPeriod;
		this.mainProcurementCategory = mainProcurementCategory;
		this.additionalProcurementCategories = additionalProcurementCategories;
		this.procurementMethod = procurementMethod;
		this.procurementMethodDetails = procurementMethodDetails;
		this.procuringEntity = procuringEntity;
		this.criteria = criteria;
		this.lots = lots;
		this.notifiedSuppliers = notifiedSuppliers;
		this.items = items;
		this.tenderers = tenderers;
		this.documents = documents;
		this.numberOfTenderers = numberOfTenderers;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("title")
	public void setTitle(String title) {
		this.title = title;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("awardCriteria")
	public String getAwardCriteria() {
		return awardCriteria;
	}

	@JsonProperty("awardCriteria")
	public void setAwardCriteria(String awardCriteria) {
		this.awardCriteria = awardCriteria;
	}

	@JsonProperty("awardCriteriaDetails")
	public String getAwardCriteriaDetails() {
		return awardCriteriaDetails;
	}

	@JsonProperty("awardCriteriaDetails")
	public void setAwardCriteriaDetails(String awardCriteriaDetails) {
		this.awardCriteriaDetails = awardCriteriaDetails;
	}

	@JsonProperty("submissionMethod")
	public List<String> getSubmissionMethod() {
		return submissionMethod;
	}

	@JsonProperty("submissionMethod")
	public void setSubmissionMethod(List<String> submissionMethod) {
		this.submissionMethod = submissionMethod;
	}

	@JsonProperty("bidOpening")
	public BidOpening getBidOpening() {
		return bidOpening;
	}

	@JsonProperty("bidOpening")
	public void setBidOpening(BidOpening bidOpening) {
		this.bidOpening = bidOpening;
	}

	@JsonProperty("submissionMethodDetails")
	public String getSubmissionMethodDetails() {
		return submissionMethodDetails;
	}

	@JsonProperty("submissionMethodDetails")
	public void setSubmissionMethodDetails(String submissionMethodDetails) {
		this.submissionMethodDetails = submissionMethodDetails;
	}

	@JsonProperty("eligibilityCriteria")
	public String getEligibilityCriteria() {
		return eligibilityCriteria;
	}

	@JsonProperty("eligibilityCriteria")
	public void setEligibilityCriteria(String eligibilityCriteria) {
		this.eligibilityCriteria = eligibilityCriteria;
	}

	@JsonProperty("statusDetails")
	public String getStatusDetails() {
		return statusDetails;
	}

	@JsonProperty("statusDetails")
	public void setStatusDetails(String statusDetails) {
		this.statusDetails = statusDetails;
	}

	@JsonProperty("enquiriesAddress")
	public EnquiriesAddress getEnquiriesAddress() {
		return enquiriesAddress;
	}

	@JsonProperty("enquiriesAddress")
	public void setEnquiriesAddress(EnquiriesAddress enquiriesAddress) {
		this.enquiriesAddress = enquiriesAddress;
	}

	@JsonProperty("mainProcurementCategoryDetails")
	public String getMainProcurementCategoryDetails() {
		return mainProcurementCategoryDetails;
	}

	@JsonProperty("mainProcurementCategoryDetails")
	public void setMainProcurementCategoryDetails(String mainProcurementCategoryDetails) {
		this.mainProcurementCategoryDetails = mainProcurementCategoryDetails;
	}

	@JsonProperty("hasEnquiries")
	public Boolean getHasEnquiries() {
		return hasEnquiries;
	}

	@JsonProperty("hasEnquiries")
	public void setHasEnquiries(Boolean hasEnquiries) {
		this.hasEnquiries = hasEnquiries;
	}

	@JsonProperty("value")
	public Value getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(Value value) {
		this.value = value;
	}

	@JsonProperty("datePublished")
	public String getDatePublished() {
		return datePublished;
	}

	@JsonProperty("datePublished")
	public void setDatePublished(String datePublished) {
		this.datePublished = datePublished;
	}

	@JsonProperty("tenderPeriod")
	public TenderPeriod getTenderPeriod() {
		return tenderPeriod;
	}

	@JsonProperty("tenderPeriod")
	public void setTenderPeriod(TenderPeriod tenderPeriod) {
		this.tenderPeriod = tenderPeriod;
	}

	@JsonProperty("awardPeriod")
	public AwardPeriod getAwardPeriod() {
		return awardPeriod;
	}

	@JsonProperty("awardPeriod")
	public void setAwardPeriod(AwardPeriod awardPeriod) {
		this.awardPeriod = awardPeriod;
	}

	@JsonProperty("enquiryPeriod")
	public EnquiryPeriod getEnquiryPeriod() {
		return enquiryPeriod;
	}

	@JsonProperty("enquiryPeriod")
	public void setEnquiryPeriod(EnquiryPeriod enquiryPeriod) {
		this.enquiryPeriod = enquiryPeriod;
	}

	@JsonProperty("mainProcurementCategory")
	public String getMainProcurementCategory() {
		return mainProcurementCategory;
	}

	@JsonProperty("mainProcurementCategory")
	public void setMainProcurementCategory(String mainProcurementCategory) {
		this.mainProcurementCategory = mainProcurementCategory;
	}

	@JsonProperty("additionalProcurementCategories")
	public List<String> getAdditionalProcurementCategories() {
		return additionalProcurementCategories;
	}

	@JsonProperty("additionalProcurementCategories")
	public void setAdditionalProcurementCategories(List<String> additionalProcurementCategories) {
		this.additionalProcurementCategories = additionalProcurementCategories;
	}

	@JsonProperty("procurementMethod")
	public String getProcurementMethod() {
		return procurementMethod;
	}

	@JsonProperty("procurementMethod")
	public void setProcurementMethod(String procurementMethod) {
		this.procurementMethod = procurementMethod;
	}

	@JsonProperty("procurementMethodDetails")
	public String getProcurementMethodDetails() {
		return procurementMethodDetails;
	}

	@JsonProperty("procurementMethodDetails")
	public void setProcurementMethodDetails(String procurementMethodDetails) {
		this.procurementMethodDetails = procurementMethodDetails;
	}

	@JsonProperty("procuringEntity")
	public ProcuringEntity getProcuringEntity() {
		return procuringEntity;
	}

	@JsonProperty("procuringEntity")
	public void setProcuringEntity(ProcuringEntity procuringEntity) {
		this.procuringEntity = procuringEntity;
	}

	@JsonProperty("criteria")
	public List<Criterium> getCriteria() {
		return criteria;
	}

	@JsonProperty("criteria")
	public void setCriteria(List<Criterium> criteria) {
		this.criteria = criteria;
	}

	@JsonProperty("lots")
	public List<Lot> getLots() {
		return lots;
	}

	@JsonProperty("lots")
	public void setLots(List<Lot> lots) {
		this.lots = lots;
	}

	@JsonProperty("notifiedSuppliers")
	public List<NotifiedSupplier> getNotifiedSuppliers() {
		return notifiedSuppliers;
	}

	@JsonProperty("notifiedSuppliers")
	public void setNotifiedSuppliers(List<NotifiedSupplier> notifiedSuppliers) {
		this.notifiedSuppliers = notifiedSuppliers;
	}

	@JsonProperty("items")
	public List<Item> getItems() {
		return items;
	}

	@JsonProperty("items")
	public void setItems(List<Item> items) {
		this.items = items;
	}

	@JsonProperty("tenderers")
	public List<Tenderer> getTenderers() {
		return tenderers;
	}

	@JsonProperty("tenderers")
	public void setTenderers(List<Tenderer> tenderers) {
		this.tenderers = tenderers;
	}

	@JsonProperty("documents")
	public List<Document> getDocuments() {
		return documents;
	}

	@JsonProperty("documents")
	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	@JsonProperty("numberOfTenderers")
	public Integer getNumberOfTenderers() {
		return numberOfTenderers;
	}

	@JsonProperty("numberOfTenderers")
	public void setNumberOfTenderers(Integer numberOfTenderers) {
		this.numberOfTenderers = numberOfTenderers;
	}

	@JsonProperty("enquiries")
	public List<Enquiry> getEnquiries() {
		return enquiries;
	}

	@JsonProperty("enquiries")
	public void setEnquiries(List<Enquiry> enquiries) {
		this.enquiries = enquiries;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
