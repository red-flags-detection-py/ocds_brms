
package com.ocds.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "sourceSystem",
    "value",
    "date",
    "payer",
    "payee",
    "requestDate",
    "finantialObligations",
    "financialCode"
})
public class Transaction {

    @JsonProperty("id")
    private String id;
    @JsonProperty("sourceSystem")
    private String sourceSystem;
    @JsonProperty("value")
    private Value value;
    @JsonProperty("date")
    private String date;
    @JsonProperty("payer")
    private Payer payer;
    @JsonProperty("payee")
    private Payee payee;
    @JsonProperty("requestDate")
    private String requestDate;
    @JsonProperty("finantialObligations")
    private List<FinantialObligation> finantialObligations = null;
    @JsonProperty("financialCode")
    private String financialCode;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Transaction() {
    }

    /**
     * 
     * @param date
     * @param payee
     * @param finantialObligations
     * @param sourceSystem
     * @param requestDate
     * @param financialCode
     * @param id
     * @param value
     * @param payer
     */
    public Transaction(String id, String sourceSystem, Value value, String date, Payer payer, Payee payee, String requestDate, List<FinantialObligation> finantialObligations, String financialCode) {
        super();
        this.id = id;
        this.sourceSystem = sourceSystem;
        this.value = value;
        this.date = date;
        this.payer = payer;
        this.payee = payee;
        this.requestDate = requestDate;
        this.finantialObligations = finantialObligations;
        this.financialCode = financialCode;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("sourceSystem")
    public String getSourceSystem() {
        return sourceSystem;
    }

    @JsonProperty("sourceSystem")
    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    @JsonProperty("value")
    public Value getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(Value value) {
        this.value = value;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("payer")
    public Payer getPayer() {
        return payer;
    }

    @JsonProperty("payer")
    public void setPayer(Payer payer) {
        this.payer = payer;
    }

    @JsonProperty("payee")
    public Payee getPayee() {
        return payee;
    }

    @JsonProperty("payee")
    public void setPayee(Payee payee) {
        this.payee = payee;
    }

    @JsonProperty("requestDate")
    public String getRequestDate() {
        return requestDate;
    }

    @JsonProperty("requestDate")
    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    @JsonProperty("finantialObligations")
    public List<FinantialObligation> getFinantialObligations() {
        return finantialObligations;
    }

    @JsonProperty("finantialObligations")
    public void setFinantialObligations(List<FinantialObligation> finantialObligations) {
        this.finantialObligations = finantialObligations;
    }

    @JsonProperty("financialCode")
    public String getFinancialCode() {
        return financialCode;
    }

    @JsonProperty("financialCode")
    public void setFinancialCode(String financialCode) {
        this.financialCode = financialCode;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
